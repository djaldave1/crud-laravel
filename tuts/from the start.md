create laravel:
`laravel new laravel8-crud`

#### inside the folder
`composer require laravel/ui`

#### to add assets bootstrap
* `php artisan ui bootstrap`
* `npm install && npm run dev`


### edit the .env
* comment  `#DB_HOST=mysql`
* uncomment `DB_HOST=127.0.0.1`

### create database name
`laravel8_crud`

### to clear and cache config
`php artisan config:cache`

### model
php artisan make:model Post -m

### add this code in `app/Models/Post.php`
```
 protected $fillable =[
        'title','description','price'
    ];
	
```

## edit the function and add this code in `database/migrations/2020_12_11_030104_create_posts_table.php`
 
``` 
  public function up()
     {
         Schema::create('posts', function (Blueprint $table) {
             $table->id();
             $table->string("title");
             $table->string("description");
             $table->double("price");
             $table->timestamps();
         });
     }
```

#migration 
`php artisan migrate:fresh`


#controller
`php artisan make:controller CrudController`


## in resources/views create folder `layout` and `post`
* #### create file under the folder layout
* `app.blade.php`
  * add this code : 
```
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset("css/app.css")}}">
    <title>@yield("title")</title>
</head>
<body style="background-color: #222831;">
<nav class="navbar-dark bg-dark navbar">
    <div class="container">
        <a href="{{route("index.index")}}" class="mx-auto btn border-warning btn-outline-warning btn-secondary" style="font-size: 1.125rem">Home</a>
    </div>
</nav>
<div class="container pt-3">
    @yield("content")
</div>
<script src="{{asset("js/app.js")}}"></script>
<script>
    $(function () {
        $(".alert-success").fadeOut(3000);
    })
</script>
</body>
</html>

```
  

* ### create file under the folder `posts`
* `create.blade.php`
  * add this `code` : 
```
@extends("layouts.app")
@section("title", "Create")
@section("content")
<div class="row">
    <div class="mx-auto col-lg-10">
        <a href="{{route("index.index")}}" class="btn btn-outline-warning btn-secondary">&laquo; Back to Menu</a>
        <h2 class="text-center text-warning m-3">Create</h2>
        @if($errors->any())
            <div class="alert alert-danger mt-2">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="post" action="{{ route('index.store') }}" autocomplete="off" class="text-warning">
            @csrf
            <div class="modal-body">
                <div class="form-group">
                    <label for="post-title">Name</label>
                    <input type="text" name="title"
                           value="{{ old('title') }}" class="form-control  text-center" id="post-title" style="font-size: 1.15rem">
                </div>
                <div class="form-group">
                    <label for="post-description">Description</label>
                    <textarea name="description" class="form-control  text-center " id="post-description" rows="3"  style="font-size: 1.15rem">{{ old('description') }}</textarea>
                </div>
                <div class="form-group">
                    <label for="post-price">Price</label>
                    <input type="text" name="price"
                           value="{{ old('price') }}" class="form-control text-center" id="post-price" style="font-size: 1.15rem"   >
                </div>
            </div>
            <div class="modal-footer border-top-0 d-flex justify-content-center">
                <button type="submit" class="btn btn-success w-50" name="submit">Submit</button>
            </div>
        </form>
    </div>
</div>
@endsection

```
* `edit.blade.php`
   * add this `code` : 

```
@extends("layouts.app")
@section("title", "Edit")
@section("content")
    <div class="row">
        <div class="mx-auto col-lg-10">
            <a href="{{route("index.index")}}" class="btn btn-outline-warning btn-secondary">&laquo; Back to Menu</a>
            <h2 class="text-center text-warning m-3">Update Details</h2>
            @if($errors->any())
                <div class="alert alert-danger mt-2">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form method="post" action="{{ route('index.update', $post) }}" autocomplete="off" class="text-warning">
                @csrf
                @method("PATCH")
                <div class="modal-body">
                    <div class="form-group">
                        <label for="post-title">Name</label>
                        <input type="text" name="title"
                               value="{{$post->title }}" class="form-control  text-center" id="post-title" style="font-size: 1.15rem">
                    </div>
                    <div class="form-group">
                        <label for="post-description">Description</label>
                        <textarea name="description" class="form-control  text-center " id="post-description" rows="3"  style="font-size: 1.15rem">{{ $post->description}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="post-price">Price</label>
                        <input type="text" name="price"
                               value="{{ $post->price }}" class="form-control text-center" id="post-price" style="font-size: 1.15rem"   >
                    </div>
                </div>
                <div class="modal-footer border-top-0 d-flex justify-content-center">
                    <button type="submit" class="btn btn-success w-50" name="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection

```

* `index.blade.php`
  * add this `code` : 
```
@extends("layouts.app")

@section("title",'HomePage')

@section("content")
<div class="row">
    <div class="mx-auto col-lg-10">
        <a href="{{route("index.create")}}" class="btn btn-outline-warning btn-secondary">Create a Post</a>
        @if(session()->get("success"))
            <div class="alert alert-success mt-3">
                {{session()->get("success")}}
            </div>
        @endif
        <table class="table-responsive-md table table-striped text-warning table-dark mt-2">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th class="">Description</th>
                    <th>Price</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                @foreach($index as $post)
                    <tr>
                        <td class="w-auto">{{$post->id}}</td>
                        <td class="w-auto">{{$post->title}}</td>
                        <td class="w-auto ">{{$post->description}}</td>
                        <td class="w-auto">{{$post->price}}</td>
                        <td class="w-auto"><a href="{{route("index.show", $post)}}" class="btn btn-sm btn-outline-success  w-100"><i class="fa fa-eye"></i></a></td>
                        <td class="w-auto"><a href="{{route("index.edit", $post)}}" class="btn btn-outline-secondary btn-sm w-100"><i class="fa fa-pencil"></i></a></td>
                        <td class="w-auto">
                            <form method="post" action="{{ route('index.destroy', $post) }}">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-outline-danger btn-sm w-100"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

```
* `show.blade.php`
  * add this `code` : 

```
@extends("layouts.app")
@section("title", "Create")
@section("content")
    <div class="row">
        <div class="mx-auto col-lg-10">
            <a href="{{route("index.index")}}" class="btn btn-outline-warning btn-secondary">&laquo; Back to Menu</a>
            <h2 class="text-center text-warning m-3">View Details</h2>
            <form readonly autocomplete="off" class="text-warning">
                @csrf
                <div class="">
                    <div class="form-group">
                        <label for="post-title">Name</label>
                        <input type="text" name="title"
                               value="{{$post->title}}" readonly disabled class="form-control bg-dark text-center text-white font-weight-bold" id="post-title"  style="font-size: 1.15rem">
                    </div>
                    <div class="form-group">
                        <label for="post-description">Description</label>
                        <textarea name="description" class="form-control bg-dark text-center text-white font-weight-bold" readonly disabled id="post-description" rows="2"  style="font-size: 1.15rem">{{$post->description}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="post-price">Price</label>
                        <input type="text" name="price" readonly disabled
                               value="{{$post->price}}" class="form-control bg-dark text-white text-center font-weight-bold" id="post-price"  style="font-size: 1.15rem">
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

```



## in app/Http/Controllers/CrudController.php
```
<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class CrudController extends Controller
{
    //
    public function index(){
        $index  = Post::all();
        return view("posts.index", compact("index"));
    }

    public function create(){
        return view("posts.create");
    }

    public function store(Request $request){
        $request->validate([
            'title'=> 'required|max:255',
            'description'=> 'required|max:255',
            'price'=> 'required|max:255'
        ]);
        $post = new Post([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'price' => $request->get('price'),
        ]);
        $post->save();
        return redirect('/index')->with('success', 'Post added successfully!');
    }

    public function show($id){
        $post = Post::find($id);
        return view('posts.show', compact('post'));
    }

    public function edit($id){
        $post = Post::find($id);
        return view('posts.edit', compact('post'));
    }
    public function update(Request $request, $id){
        $request->validate([
            'title'=> 'required|max:255',
            'description'=> 'required|max:255',
            'price'=> 'required|max:255'
        ]);
        $post = Post::find($id);
        $post->title = $request->get('title');
        $post->description = $request->get('description');
        $post->price = $request->get('price');
        $post->save();
        return redirect('/index')->with('success', 'Post edited successfully!');
    }
    public function destroy($id){
        $post = Post::find($id);
        $post->delete();
        return redirect('/index')->with('success', 'Post deleted!');
    }
}
```

## add this app/Models/Post.php

```

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static find($id)
 */
class Post extends Model
{
    use HasFactory;
    protected $fillable =[
        'title','description','price'
    ];
}

```

## add routes in routes/web.php

```

Route::resource("index", "App\Http\Controllers\CrudController");
Route::resource("/", "App\Http\Controllers\CrudController");

```
